package com.mars.module.admin.service;

import com.mars.module.admin.entity.SysDictData;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.SysDictDataRequest;

import java.util.List;

/**
 * 字典数据接口
 *
 * @author mars
 * @date 2023-11-18
 */
public interface ISysDictDataService {
    /**
     * 新增
     *
     * @param param param
     * @return SysDictData
     */
    SysDictData add(SysDictDataRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysDictDataRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysDictData
     */
    SysDictData getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysDictData>
     */
    PageInfo<SysDictData> pageList(SysDictDataRequest param);

    /**
     * 通过类型获取字典列表
     *
     * @param type 类型名称
     * @return List<SysDictData>
     */
    List<SysDictData> getDictData(String type);
}
