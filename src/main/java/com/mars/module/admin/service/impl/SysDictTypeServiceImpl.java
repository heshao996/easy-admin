package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.SysDictTypeRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.SysDictTypeMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.SysDictType;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.ISysDictTypeService;

import java.util.List;

/**
 * 字典类型业务层处理
 *
 * @author mars
 * @date 2023-11-18
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysDictTypeServiceImpl implements ISysDictTypeService {

    private final SysDictTypeMapper baseMapper;

    @Override
    public SysDictType add(SysDictTypeRequest request) {
        SysDictType entity = new SysDictType();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean update(SysDictTypeRequest request) {
        SysDictType entity = new SysDictType();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public SysDictType getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<SysDictType> pageList(SysDictTypeRequest request) {
        Page<SysDictType> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<SysDictType> query = this.buildWrapper(request);
        IPage<SysDictType> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    @Override
    public List<SysDictType> list() {
        return baseMapper.selectList(Wrappers.lambdaQuery(SysDictType.class).eq(SysDictType::getState, 0));
    }

    private LambdaQueryWrapper<SysDictType> buildWrapper(SysDictTypeRequest param) {
        LambdaQueryWrapper<SysDictType> query = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getName())) {
            query.like(SysDictType::getName, param.getName());
        }
        if (StringUtils.isNotBlank(param.getType())) {
            query.like(SysDictType::getType, param.getType());
        }
        return query;
    }

}
