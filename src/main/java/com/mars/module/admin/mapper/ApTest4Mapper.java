package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApTest4;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 测试4Mapper接口
 *
 * @author mars
 * @date 2023-12-22
 */
public interface ApTest4Mapper extends BasePlusMapper<ApTest4> {

}
